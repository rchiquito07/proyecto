package com.android.rssmovil20;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.rssmovil20.Adapter.FeedAdapter;
import com.android.rssmovil20.Common.HTTPDataHandler;
import com.android.rssmovil20.Model.RSSObject;
import com.google.gson.Gson;

public class rss_3 extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;
    RecyclerView recyclerView;
    RSSObject rssObject;
    private SwipeRefreshLayout swipeRefreshLayout;

    //RSS Link

    private final String RSS_link = "https://lahora.com.ec/rss";

    private final String RSS_to_Json_API = " https://api.rss2json.com/v1/api.json?rss_url=";

    public static  final  String apiKey = "&api_key=at0omtysfd0fopsbkzur7jdlaqop0kstc2le8xlm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_1);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("La Hora");
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()){
            onLoadingRefresh();
        } else {
            Toast.makeText(getApplicationContext(), "Ups... Algo salio mal, comprueba tu conexion e intenta otra vez", Toast.LENGTH_LONG).show();
        }
    }

    private void loadRSS() {
        swipeRefreshLayout.setRefreshing(true);
        AsyncTask<String, String, String> loadRSSAsync = new AsyncTask<String, String, String>() {

            ProgressDialog mDialog = new ProgressDialog(rss_3.this);

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setRefreshing(true);
                mDialog.setTitle("RSS");
                mDialog.setMessage("Por favor espere ...");
                mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mDialog.setCancelable(false);
                mDialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                String result;
                HTTPDataHandler http = new HTTPDataHandler();
                result = http.GetHTTPData(params[0]);
                return result;
            }


            @Override
            protected void onPostExecute(String s) {
                mDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                rssObject = new Gson().fromJson(s, RSSObject.class);
                FeedAdapter adapter = new FeedAdapter(rssObject, getBaseContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        };

        StringBuilder url_get_data = new StringBuilder(RSS_to_Json_API);
        url_get_data.append(RSS_link + apiKey);
        loadRSSAsync.execute(url_get_data.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh)
            swipeRefreshLayout.setRefreshing(true);
        onLoadingRefresh();
        return true;
    }

    @Override
    public void onRefresh() {
        loadRSS();
    }
    private void onLoadingRefresh(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    loadRSS();
                }
            });
        } else {
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getApplicationContext(), "Ups... Algo salio mal, comprueba tu conexion e intenta otra vez", Toast.LENGTH_LONG).show();
        }
    }
}
