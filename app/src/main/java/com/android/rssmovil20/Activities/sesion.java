package com.android.rssmovil20.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.rssmovil20.Preferences;
import com.android.rssmovil20.R;
import com.android.rssmovil20.SessionManager;
import com.android.rssmovil20.fragmentos.contenedorFragment;
import com.android.rssmovil20.fragmentos.principalFragment;
import com.android.rssmovil20.homeActivity;
import com.android.rssmovil20.principalActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class sesion extends AppCompatActivity {

    private TextView registrate;
    private Button buttonIngresar, buttonInvitado;
    private EditText email, password;
    private ProgressBar loading;
    private RadioButton rbsesion;
    private static String URL_LOGIN = "http://192.168.0.104:80/android_login_register/login.php";
    SessionManager sessionManager;
    private boolean isActivateRadioButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion);

        sessionManager = new SessionManager(this);

        registrate = (TextView) findViewById(R.id.registrate);
        loading = findViewById(R.id.loading);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        buttonIngresar = (Button) findViewById(R.id.buttonIngresar);
        buttonInvitado = (Button) findViewById(R.id.buttonInvitado);
        rbsesion = findViewById(R.id.sesionNo);

        isActivateRadioButton = rbsesion.isChecked();

        if(Preferences.obtenerPreferenceBoolean(this, Preferences.PREFERENCE_ESTADO_BUTTON_SESION)){
            Intent intent = new Intent(sesion.this, homeActivity.class);
            startActivity(intent);
            finish();
        }

        rbsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isActivateRadioButton){
                    rbsesion.setChecked(false);
                }
                isActivateRadioButton = rbsesion.isChecked();
            }
        });

        buttonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mEmail = email.getText().toString().trim();
                String mPass = password.getText().toString().trim();

                if (mEmail.length() != 0 && mPass.length() != 0){
                   // Toast.makeText(sesion.this, "Usuario y/o Contraseña Incorrectos", Toast.LENGTH_SHORT).show();
                }
                if (!mEmail.isEmpty() || !mPass.isEmpty()){
                    Login(mEmail, mPass);
                    //Toast.makeText(getApplicationContext(),"Bienvenido a Noticias RSS",Toast.LENGTH_SHORT).show();
                }
                else {
                    email.setError("Inserte Correo");
                    password.setError("Inserte Contraseña");
                }
                email.setText("");
                password.setText("");
                email.findFocus();

            }
        });

        buttonInvitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sesion.this, principalActivity.class);
                startActivity(intent);
                finish();
            }
        });

        registrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sesion.this, registro_usuario.class);
                startActivity(intent);
                finish();
            }
        });
    }



    private static final int INTERVALO = 3000;
    private long tiempoPrimerClick;

    @Override
    public void onBackPressed() {
        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, "       No puedes regresar\nVuelve a presionar para salir", Toast.LENGTH_LONG).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }

    private void Login(final String email, final String password){
        loading.setVisibility(View.VISIBLE);
        buttonIngresar.setVisibility(View.GONE);
        registrate.setVisibility(View.GONE);
        buttonInvitado.setVisibility(View.GONE);
        rbsesion.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Preferences.savePreferenceBoolean(sesion.this, rbsesion.isChecked(), Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
                        Preferences.savePreferendceString(sesion.this, email, Preferences.PREFERENCE_USUARIO_LOGIN);
                        Preferences.savePreferendceString(sesion.this, password, Preferences.PREFERENCE_PASSWORD_LOGIN);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("login");
                            if (success.equals("1")) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String nombre = object.getString("nombre").trim();
                                    String email = object.getString("email").trim();
                                    String id = object.getString("id").trim();

                                    sessionManager.crateSession(nombre, email, id);

                                    Intent intent = new Intent(sesion.this, homeActivity.class);
                                    intent.putExtra("nombre", nombre);
                                    intent.putExtra("email", email);
                                    startActivity(intent);
                                    finish();

                                    loading.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loading.setVisibility(View.GONE);
                            registrate.setVisibility(View.VISIBLE);
                            buttonIngresar.setVisibility(View.VISIBLE);
                            buttonInvitado.setVisibility(View.VISIBLE);
                            rbsesion.setVisibility(View.VISIBLE);
                            Toast.makeText(sesion.this, "Correo y/o Contraseña Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                        buttonIngresar.setVisibility(View.VISIBLE);
                        buttonInvitado.setVisibility(View.VISIBLE);
                        registrate.setVisibility(View.VISIBLE);
                        rbsesion.setVisibility(View.VISIBLE);
                        Toast.makeText(sesion.this, "Error al conectar con el servidor" , Toast.LENGTH_SHORT).show();  /* + error.toString()*/
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
