package com.android.rssmovil20.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.rssmovil20.R;
import com.android.rssmovil20.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class registro_usuario extends AppCompatActivity {

    private TextView sesion1;
    private Button btnRegistro;
    private EditText Nombre, Email, Password, c_Password;
    private ProgressBar loading;
    private static String URL_REGIST = "http://192.168.0.104:80/android_login_register/register.php" ;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        sessionManager = new SessionManager(this);

        sesion1 = (TextView) findViewById(R.id.sesion);
        loading = (ProgressBar) findViewById(R.id.loading);
        btnRegistro = (Button) findViewById(R.id.buttonRegistro);
        Nombre = (EditText) findViewById(R.id.Nombre);
        Email = (EditText) findViewById(R.id.Email);
        Password = (EditText) findViewById(R.id.Password);
        //c_Password = (EditText) findViewById(R.id.c_Password);


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registro();

            }
        });

        sesion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(registro_usuario.this, sesion.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private static final int INTERVALO = 3000;
    private long tiempoPrimerClick;

    @Override
    public void onBackPressed() {
        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, "       No puedes regresar\nVuelve a presionar para salir", Toast.LENGTH_LONG).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }

    private void registro(){
        loading.setVisibility(View.VISIBLE);
        btnRegistro.setVisibility(View.GONE);
        sesion1.setVisibility(View.GONE);

        final String Nombre = this.Nombre.getText().toString().trim();
        final String Email = this.Email.getText().toString().trim();
        final String Password = this.Password.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGIST,
                new Response.Listener<String>() {
                @Override
                    public void onResponse(String response) {
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                            if (success.equals("1")) {

                                Toast.makeText(registro_usuario.this, "Registro Exitoso", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(registro_usuario.this, sesion.class);
                                startActivity(intent);
                                finish();
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(registro_usuario.this, "Error al registrar", Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                        sesion1.setVisibility(View.VISIBLE);
                        btnRegistro.setVisibility(View.VISIBLE);
                    }
                }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(registro_usuario.this, "Error al registrar, no se pudo establecer la conexion", Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                        btnRegistro.setVisibility(View.VISIBLE);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", Nombre);
                params.put("email", Email);
                params.put("password", Password);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
