package com.android.rssmovil20.Sesion;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.rssmovil20.Adapter.seccionesAdapter;
import com.android.rssmovil20.Clases.Utilidades;
import com.android.rssmovil20.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link content_fragm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link content_fragm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class content_fragm extends Fragment {

    private OnFragmentInteractionListener mListener;

    View vista;
    private AppBarLayout appBar;
    private TabLayout pestanias;
    private ViewPager viewPager;

    public content_fragm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment content_fragm.
     */
    // TODO: Rename and change types and number of parameters
    public static content_fragm newInstance(String param1, String param2) {
        content_fragm fragment = new content_fragm();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista = inflater.inflate(R.layout.fragment_content_fragm, container, false);

        if (Utilidades.rotacion == 0) {
        View parent = (View) container.getParent();
        if (appBar == null) {
            appBar = (AppBarLayout) parent.findViewById(R.id.appBar);
            pestanias = new TabLayout(getActivity());
            pestanias.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));
            appBar.addView(pestanias);

            viewPager = (ViewPager) vista.findViewById(R.id.idInformacion);
            llevarViewPager(viewPager);
            viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                }
            });
            pestanias.setupWithViewPager(viewPager);
        }
            pestanias.setTabGravity(TabLayout.GRAVITY_FILL);
        } else {
            Utilidades.rotacion = 1;
        }

        return vista;
    }

        private void llevarViewPager(ViewPager viewPager) {
        seccionesAdapter adapter = new seccionesAdapter(getFragmentManager());
        adapter.addFragment(new principal_ini(), "NACIONAL");
        adapter.addFragment(new principal_2_ini(), "INTERNACIONAL");

        viewPager.setAdapter(adapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Utilidades.rotacion == 0) {
            appBar.removeView(pestanias);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}



