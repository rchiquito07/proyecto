package com.android.rssmovil20.Sesion;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.rssmovil20.Activities.sesion;
import com.android.rssmovil20.Preferences;
import com.android.rssmovil20.R;
import com.android.rssmovil20.SessionManager;
import com.android.rssmovil20.fragmentos.contenedorFragment_2;
import com.android.rssmovil20.fragmentos.inicioFragment;
import com.android.rssmovil20.fragmentos.principalFragment;
import com.android.rssmovil20.fragmentos.principal_2_Fragment;
import com.android.rssmovil20.perfilActivity;
import com.android.rssmovil20.principalActivity;

public class sesionIni extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, principal_ini.OnFragmentInteractionListener, principal_2_ini.OnFragmentInteractionListener,content_fragm.OnFragmentInteractionListener {

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion_ini);

        sessionManager = new SessionManager(this);
        sessionManager.checkLoggin();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "RSS", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState==null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, new content_fragm()).commit();
            navigationView.setCheckedItem(R.id.nav_mFacil);

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sesion_ini, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.perfil) {
            Intent i = new Intent(getApplicationContext(), perfilActivity.class);
            startActivity(i);
            finish();
            return true;
        }

        sessionManager = new SessionManager(this);
        sessionManager.checkLoggin();

        if (id == R.id.logout){
            Preferences.savePreferenceBoolean(this, false, Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
            sessionManager.logout2();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){
            case R.id.nav_principal:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_main, new principal_ini()).commit();
                break;
            case R.id.nav_principal_2:
               getSupportFragmentManager().beginTransaction().replace(R.id.content_main, new principal_2_ini()).commit();
                break;
            case R.id.nav_mFacil:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_main, new content_fragm()).commit();
                break;
            case R.id.nav_send:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
