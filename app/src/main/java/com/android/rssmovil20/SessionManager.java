package com.android.rssmovil20;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.rssmovil20.Activities.sesion;
import com.android.rssmovil20.Sesion.sesionIni;

import java.util.HashMap;

public class SessionManager {

    SharedPreferences sharedPreference;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;

    private static final String PREFE_MODE = "LOGIN";
    private static final String LOGIN = "IS LOGIN";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String ID = "ID";

    public SessionManager (Context context){
        this.context = context;
        sharedPreference = context.getSharedPreferences(PREFE_MODE, PRIVATE_MODE);
        editor = sharedPreference.edit();
    }

    public void crateSession(String name, String email, String id){
        editor.putBoolean(LOGIN, true);
        editor.putString(NAME, name);
        editor.putString(EMAIL, email);
        editor.putString(ID, id);
        editor.apply();
    }

    public boolean isLoggin(){
        return sharedPreference.getBoolean(LOGIN, false);
    }

    public void checkLoggin(){

        if (!this.isLoggin()){
            Intent i = new Intent(context, sesion.class);
            context.startActivity(i);
            ((homeActivity) context).finish();
        }
    }

    public HashMap<String, String> getUserDetail(){
        HashMap<String, String> user = new HashMap<>();
        user.put(NAME, sharedPreference.getString(NAME, null));
        user.put(EMAIL, sharedPreference.getString(EMAIL, null));
        user.put(ID, sharedPreference.getString(ID, null));

        return user;
    }

    public void logout(){

        editor.clear();
        editor.commit();
        Preferences.savePreferenceBoolean(context, false, Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
        Intent i = new Intent(context, sesion.class);
        context.startActivity(i);
        ((homeActivity) context).finish();

    }

    public void logout2(){

        editor.clear();
        editor.commit();
        Preferences.savePreferenceBoolean(context, false, Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
        Intent i = new Intent(context, sesion.class);
        context.startActivity(i);
        ((sesionIni) context).finish();

    }
}
