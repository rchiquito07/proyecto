package com.android.rssmovil20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.rssmovil20.Activities.sesion;
import com.android.rssmovil20.Sesion.sesionIni;

import java.util.HashMap;

public class homeActivity extends AppCompatActivity {

    private TextView nombre, email;
    private Button btn_logout, btn_principal;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sessionManager = new SessionManager(this);
        sessionManager.checkLoggin();

        nombre = findViewById(R.id.nombre);
        email = findViewById(R.id.email);
        btn_logout = findViewById(R.id.btn_logout);
        btn_principal = findViewById(R.id.btn_principal);

        HashMap<String, String> user = sessionManager.getUserDetail();
        String mNombre = user.get(sessionManager.NAME);
        String mEmail = user.get(sessionManager.EMAIL);

        nombre.setText(mNombre);
        email.setText(mEmail);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.savePreferenceBoolean(homeActivity.this, false, Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
                sessionManager.logout();
            }
        });

        btn_principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homeActivity.this, sesionIni.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
