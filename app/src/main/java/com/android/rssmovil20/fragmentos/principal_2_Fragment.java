package com.android.rssmovil20.fragmentos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.rssmovil20.ABC.ABC;
import com.android.rssmovil20.R;
import com.android.rssmovil20.elClarin.elClarin;
import com.android.rssmovil20.elComercioP.elComercioP;
import com.android.rssmovil20.elMundo.elMundo;
import com.android.rssmovil20.elPais.elPais;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link principal_2_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link principal_2_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class principal_2_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public principal_2_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment principal_2_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static principal_2_Fragment newInstance(String param1, String param2) {
        principal_2_Fragment fragment = new principal_2_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Button buttonPeriodico1, buttonPeriodico2, buttonPeriodico3, buttonPeriodico4, buttonPeriodico5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_principal_2, container, false);

        buttonPeriodico1 = (Button) view.findViewById(R.id.buttonPeriodico1);
        buttonPeriodico2 = (Button) view.findViewById(R.id.buttonPeriodico2);
        buttonPeriodico3 = (Button) view.findViewById(R.id.buttonPeriodico3);
        buttonPeriodico4 = (Button) view.findViewById(R.id.buttonPeriodico4);
        buttonPeriodico5 = (Button) view.findViewById(R.id.buttonPeriodico5);

        buttonPeriodico1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ABC.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "ABC", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), elClarin.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Clarín", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), elComercioP.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Comercio de Perú", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), elMundo.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Mundo", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), elPais.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El País", Toast.LENGTH_SHORT).show();
            }

        });



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
