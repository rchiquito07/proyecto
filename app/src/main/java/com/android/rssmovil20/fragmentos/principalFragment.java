package com.android.rssmovil20.fragmentos;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.rssmovil20.R;
import com.android.rssmovil20.elExtra.extraActivity;
import com.android.rssmovil20.elTelegrafo.elTelegrafo;
import com.android.rssmovil20.elUniverso.universoActivity;
import com.android.rssmovil20.rss_1;
import com.android.rssmovil20.rss_2;
import com.android.rssmovil20.rss_3;
import com.android.rssmovil20.rss_5;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link principalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link principalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class principalFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public principalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment principalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static principalFragment newInstance(String param1, String param2) {
        principalFragment fragment = new principalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Button buttonPeriodico1, buttonPeriodico2, buttonPeriodico3, buttonPeriodico4, buttonPeriodico5, buttonPeriodico6;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_principal, container, false);

        buttonPeriodico1 = (Button) view.findViewById(R.id.buttonPeriodico1);
        buttonPeriodico2 = (Button) view.findViewById(R.id.buttonPeriodico2);
        buttonPeriodico3 = (Button) view.findViewById(R.id.buttonPeriodico3);
        buttonPeriodico4 = (Button) view.findViewById(R.id.buttonPeriodico4);
        buttonPeriodico5 = (Button) view.findViewById(R.id.buttonPeriodico5);
        buttonPeriodico6 = (Button) view.findViewById(R.id.buttonPeriodico6);

        buttonPeriodico1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), rss_1.class);
                startActivity(intent);

                    Toast.makeText(getActivity(), "El Comercio", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), universoActivity.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Universo", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), rss_3.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "La Hora", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), extraActivity.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Extra", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), elTelegrafo.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "El Telegrafo", Toast.LENGTH_SHORT).show();
            }

        });

        buttonPeriodico6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), rss_5.class);
                startActivity(intent);

                Toast.makeText(getActivity(), "ULEAM", Toast.LENGTH_SHORT).show();
            }

        });



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
