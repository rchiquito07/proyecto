package com.android.rssmovil20;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.rssmovil20.Adapter.FeedAdapter;
import com.android.rssmovil20.Adapter_2.FeedAdapter_2;
import com.android.rssmovil20.Common.HTTPDataHandler;
import com.android.rssmovil20.Common_2.HTTPDataHandler_2;
import com.android.rssmovil20.Model.RSSObject;
import com.android.rssmovil20.Model_2.RSSObject_2;
import com.google.gson.Gson;

public class rss_4 extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    RSSObject_2 rssObject;

    //RSS Link

    private final String RSS_link = "https://www.extra.ec/rss/actualidad";

    private final String RSS_to_Json_API = " https://api.rss2json.com/v1/api.json?rss_url=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_2);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("El Extra");
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        loadRSS();
    }

    private void loadRSS() {
        AsyncTask<String, String, String> loadRSSAsync = new AsyncTask<String, String, String>() {

            ProgressDialog mDialog = new ProgressDialog(rss_4.this);

            @Override
            protected void onPreExecute() {
                mDialog.setMessage("Por favor espere ...");
                mDialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                String result;
                HTTPDataHandler_2 http = new HTTPDataHandler_2();
                result = http.GetHTTPData(params[0]);
                return result;
            }


            @Override
            protected void onPostExecute(String s) {
                mDialog.dismiss();
                rssObject = new Gson().fromJson(s, RSSObject_2.class);
                FeedAdapter_2 adapter = new FeedAdapter_2(rssObject, getBaseContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        };

        StringBuilder url_get_data = new StringBuilder(RSS_to_Json_API);
        url_get_data.append(RSS_link);
        loadRSSAsync.execute(url_get_data.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_extra, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh)
            loadRSS();
        return true;
    }
}
